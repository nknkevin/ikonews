<?php

use App\Models\ArticleCategory;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Artice Categories seeder for the names
        $titles = ArticleCategory::select('name')->get();

        foreach ($titles as $title => $slug) {
            ArticleCategory::where('name', $slug->name)->update(['slug' => $slug->name]);
        }
    }
}
