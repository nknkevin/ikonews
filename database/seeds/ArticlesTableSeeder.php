<?php

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $update = new Article;
        $titles = Article::select('title')->get();

        foreach ($titles as $title => $slug) {
            Article::where('title', $slug->title)->update(['slug' => $slug->title]);
        }
    }
}
