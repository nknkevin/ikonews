
  @include('includes.header')

  @include('layouts.sidebar')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        New Quiz
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">{{$subject_name->name}}</a></li>
      <li class="active">{{$topic_name->name}}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-xs-8">
       
          {{-- array('url' => 'api/products', 'method' => 'post','enctype'=>'multipart/form-data') --}}
          {{-- {{'SubjectId'.$subject_id}}
          {{'Topic Id'.$topic_id}} --}}
          {!! Form::open(array('action' => 'QuestionsController@store','method'=>'POST','enctype'=>'multipart/form-data'))!!}
          <div class="form-group">
              {{Form::label('quiz', 'Question')}}
              {{Form::textarea('quiz', '',['class'=>'form-control','placeholder'=>'Provide the Question','required'])}}
            </div>
            <div class="form-group">
              {{Form::label('image', 'Image')}}
              {{Form::file('image')}}
            </div>
          
            <div class="form-group">
              {{Form::label('answer', 'Has Answer')}}
              {{Form::checkbox('answer', "1",true)}}
            </div>
            <div class="form-group">
              {{Form::label('answer_text', 'Answer')}}
              {{Form::textarea('answer_text', '',['class'=>'form-control','placeholder'=>'Provide the Answer of the Question'])}}
            </div>
            <div class="form-group">
              {{Form::label('answer_image', 'Answer Image')}}
              {{Form::file('answer_image')}}
            </div>
            {{Form::hidden('topic_id', $topic_id)}}

              <div class="form-group">
              {{Form::submit('Save',['class'=>'btn btn-info pull-left'])}}
            </div>
          {!! Form::close() !!} 
            
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('includes.footer')
{{-- <script type="text/javascript">
  function autoRefreshPage()
  {
      window.location = window.location.href;
  }
  setInterval('autoRefreshPage()', 5000);
</script> --}}