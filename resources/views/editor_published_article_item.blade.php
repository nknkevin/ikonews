
@include('includes.header')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

@include('layouts.sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Articles
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">IkoNews</a></li>
      <li class="active">{{$article->title}}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >Article</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <button class="btn btn-info pull-left" onclick="location.href = '/editorArticles/published';">
                  Article List</button>
                
                  <button class="btn btn-danger pull-right" data-toggle="modal" data-target="#myModal">
                    Delete Article</button>

                    <button class="btn btn-primary pull-right" style="margin-right: 5px;" onclick="location.href = '/editArticle/{{$article->id}}';">
                      Edit Article</button>

                      <button class="btn btn-success pull-right" onclick="location.href = '/sms_alert';">
                        Create News Alert</button>

                  <hr>
            
            <h3>{{$article->title}}</h3>
            <p> <i><b>Author: </b>{{$article->author}} - Created at: {{$article->created_at}}</i></p>
            <img src="{{asset('storage/article_images/'.$article->image)}}" height="200px">
            <p style="font-style: italic">{{$article->image_caption}}</p>
            <p>
              {!!$article->article_text!!}
            </p>

          </div>
        
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

{{--  --}}

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Delete An Article</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <h3 style="color: red"> Are you sure you want to delete this article ?</h3>
        
        <br>

        <p>
          <h4>
            
          {!!$article->title!!}
        </h4>
      </p>

      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-danger" onclick="location.href = '/delete/pending/{{$article->id}}';">Delete</button>
      </div>

    </div>
  </div>
</div>


@include('includes.footer')
