
  @include('includes.header')

@include('layouts.sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SMS Alerts
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">IkoNews</a></li>
      <li class="active">All SMS Alerts </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >All SMS Alerts</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <button class="btn btn-info pull-left" onclick="location.href = '/sms_alert';">
                  Create New Alert</button>
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th></th>
                <th>Title</th>
                <th>Message</th>
                <th>Status</th>
                <th>Time</th>
              
              </tr>
              </thead>
              <tbody>

              @if(count($data)>0)
                @foreach ($data as $_data)

                    <tr>
                      <td></td>
                      <td style="font-style: bold;">{{$_data->title}}
                        {{-- <a href="/editCategory/{{$_data->id}}" class=" pull-right"
                          style="margin-right: 20px;">Edit</a> --}}
                      </td>
                      <td>{{$_data->description}}</td>
                      @if ($_data->send_status==1)
                      <td style="color: #108756"><span style="font: bolder">Sent</span></td>
                      @else
                      <td style="color: #F5BD33"><span style="font: bolder">UnSent</span></td>
                      @endif
                      <td>{{\Carbon\Carbon::parse($_data->created_at)->format('H:i d F Y')}}</td>


                      <td></td> 
                    
                      </tr>

                @endforeach
              @endif
          
              </tbody>
              <tfoot>
              
              </tfoot>
            </table>


          </div>
          <!-- /.box-body -->
          @if(count($data)>1)
          {{$data->links()}}
         @endif
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

{{--  --}}




@include('includes.footer')
