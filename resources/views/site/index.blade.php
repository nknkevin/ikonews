@include('includes.website_header')


<body class="skin-orange">
    @include('includes.website_nav')
    <section class="home">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="headline">
                        {{-- <div class="nav" id="headline-nav">
                            <a class="left carousel-control" role="button" data-slide="prev">
                                <span class="ion-ios-arrow-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" role="button" data-slide="next">
                                <span class="ion-ios-arrow-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div> --}}
                        {{-- <div class="owl-carousel owl-theme" id="headline">							
                            <div class="item">
                                <a href="#"><div class="badge">BBI</div> Central Kenya allied to H.E President Uhuru Kenyatta Central Kenya allied to H.E President Uhuru Kenyatta</a>
                            </div>
                            <div class="item">
                                <a href="#">Covid-19 Positivity Rate reduces</a>
                            </div>
                        </div> --}}
                    </div>
                    <div class="owl-carousel owl-theme slide" id="featured">
                        @foreach ($featuredArticle as $item)
                        <div class="item">
                            {{-- @if($featuredArticle!= null) --}}
                            <article class="featured">
                                <div class="overlay"></div>
                                <figure>
                                    <img src="{{asset('article_images/'.$featuredArticle->image)}}" alt="Sample Article">
                                </figure>
                                <div class="details">
                                    <div class="category"><a href="/category/{{$featuredArticle->category_slug}}">{{$featuredArticle->name}}</a></div>
                                    <h1><a href="{{$featuredArticle->category_slug}}/{{$featuredArticle->article_slug}}">{{$featuredArticle->title}}</a></h1>
                                    <div class="time">{{\Carbon\Carbon::parse($featuredArticle->created_at)->format('d F Y')}}</div>
                                </div>
                            </article>
                            {{-- @endif --}}
                        </div>
                        @endforeach

                        
                    </div>
                    <div class="line">
                        <div>Latest News</div>
                    </div>
                    
                    <div class="row">
                        @php $count = 1  @endphp

                        @if(count($articleList)>0)
                        
                        @foreach ($articleList as $article)
                       
                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="row">
                                <article class="article col-md-12">
                                    <div class="inner">
                                        <figure> 
                                                                         
                                            <a href="{{$article->category_slug}}/{{$article->article_slug}}">
                                                <img src="{{asset('storage/article_images/'.$article->image)}}" height="200px" alt="Sample Article">
                                            </a>
                                        </figure>
                                        <div class="padding">
                                            <div class="detail">
                                                <div class="time">{{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</div>
                                                <div class="category"><a href="/category/{{$article->category_slug}}">{{$article->name}}</a></div>
                                            </div>
                                            <h5 style="font-size: calc(0.90em + 1vmin)"><a style="color: #1C2833;" href="{{$article->category_slug}}/{{$article->article_slug}}">{{$article->title}}</a></h5>
                                            <p>
                                                {!!Util::truncateStrings($article->article_text)!!}                                            </p>
                                            <footer>
                                                <a class="btn btn-primary more" href="{{$article->category_slug}}/{{$article->article_slug}}">
                                                    <div>More</div>
                                                    <div><i class="ion-ios-arrow-thin-right"></i></div>
                                                </a>
                                            </footer>
                                            
                                        </div>
                                    </div>
                                </article>
                                
                            </div>
                        </div>
                       
                        @endforeach
                        @if(count($articleList)>1)
                        {{$articleList->links()}}
                       @endif
                        @endif

                    </div>
                    
                    
                    <div class="banner">
                        <a href="www.kazilinks.com">
                            <img src="{{asset('website/images/ads.png')}}" alt="Sponsored">
                        </a>
                    </div>
                    <div class="line transparent little"></div>
                    <div class="row">
                        </div>
                    
                </div>
                <!-- START SIDE -->
                <div class="col-xs-6 col-md-4 sidebar" id="sidebar">
                    <div class="sidebar-title for-tablet">Sidebar</div>
                    <aside>
                        <h1 class="aside-title">Popular <a href="#" class="all">See All <i class="ion-ios-arrow-right"></i></a></h1>
                        <div class="aside-body">
                            @if(count($popular)>0)
                            @foreach ($popular as $article)
                            <article class="article-mini">
                                <div class="inner">
                                    <figure>
                                        <a href="{{$article->category_slug}}/{{$article->article_slug}}">
                                            <img src="{{asset('storage/article_images/'.$article->image)}}" alt="Sample Article">
                                        </a>
                                    </figure>
                                    <div class="padding">
                                        <h1><a href="{{$article->category_slug}}/{{$article->article_slug}}">{{$article->title}}</a></h1>
                                    </div>
                                </div>
                            </article> 
                            @endforeach
                            @endif

                            
                        </div>
                    </aside>
                    <!-- power plays -->
                    <aside>
                        <h1 class="aside-title">Power Play <a href="/category/{{$article->category_slug}}" class="all">See All <i class="ion-ios-arrow-right"></i></a></h1>
                        <div class="aside-body">
                             @if(count($powerplay)>0)
                            @foreach ($powerplay as $article) 
                             <article class="article-mini">
                                <div class="inner">
                                    <figure>
                                        <a href="{{$article->category_slug}}/{{$article->article_slug}}">
                                            <img src="{{asset('storage/article_images/'.$article->image)}}" alt="Sample Article">
                                        </a>
                                    </figure>
                                    <div class="padding">
                                        <h1><a href="{{$article->category_slug}}/{{$article->article_slug}}">{{$article->title}}</a></h1>
                                    </div>
                                </div>
                            </article> 
                            @endforeach
                            @endif 
                            
                        </div>
                    </aside>

                    <!-- sponsored -->
                        <aside id="sponsored">
                        <h1 class="aside-title">Sponsored</h1>
                        <div class="aside-body">
                            <ul class="sponsored">
                                @if(count($sponsored)>0)
                                @foreach ($sponsored as $article) 
                                <li style="width: 150px; padding:5px;">
                                    <a href="{{$article->category_slug}}/{{$article->article_slug}}">
                                        <img height="80px" width="40px" src="{{asset('storage/article_images/'.$article->image)}}" alt="Sponsored">
                                        <p style="color: royalblue;">{{$article->title}}</p>
                                    </a>
                                </li> 
                                 @endforeach
                                 @endif
                               
                            </ul>
                        </div>
                    </aside>

                    <aside>

                        <div class="aside-body">

                            <form class="newsletter" action="{{route('subscribe')}}" method="POST">
                                @csrf
                                <div class="icon">
                                    <i class="ion-ios-email-outline"></i>
                                    <h1><span>News Alert </span></h1>
                                </div>

                                <p>
                                    Get the <span style="font-weight: bolder;color:red;">latest </span> and <span style="font-weight: bolder;color:red;">breaking news </span> alerts on your phone by subscribing. 
                                </p>
                                
                                <p>
                                    Subscribe to our news alerts 
{{-- 
                                    <div class="form-group">
                                        <input type="text" name="" placeholder="Phone number" id="" class="form-control">
                                        
                                    </div> --}}
                                    <div class="form-group">

                                        {{Form::text('phone', "", ['class'=>'form-control','placeholder'=>'Phone number'])}}

                                        @if($errors->has('phone')) 

                                            <strong style="color: #ff0000">{{ $errors->first('phone') }}</strong> 
                                        
                                        @endif

                                      </div>

                                      {{-- <input type="submit" value="Subscribe"> --}}
                                      {!! Form::submit('Subscribe', ['class' =>'button btn-success']) !!}

                                </p>
                                {{-- <p>
                                    Send <span style="font-weight: bolder;color:red;">IKONEWS</span>
                                    to <span style="font-weight: bolder;color:red;">22384</span>
                                  </p>
                                  <p>By subscribing you will receive news alert on your Phone.</p> --}}
                            </form>
                        </div>

                    </aside>
                </div>

                <!-- END SIDE -->
            
            </div>
        </div>
    </section>

    


@include('includes.website_footer')