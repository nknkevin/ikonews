@include('includes.article_read_header')

	<body>
        @include('includes.website_nav')
	

		<section class="single">
			<div class="container">
				<div class="row">
					<div class="col-md-4 sidebar">
						<aside>
						  <div class="aside-body">
							<figure class="ads">
								<a href="#">
								  <img src="{{asset('website/images/ad.png')}}">
								</a>
							  <figcaption>Advertisement</figcaption>
							</figure>
						  </div>
						</aside>
						<aside>
						  <h1 class="aside-title">Latest News</h1>
						  <div class="aside-body">
							<style type="text/css" scoped>
								img {
									max-width: 100%;
									max-height: 100vh;
									margin: auto;
								}
						</style>
							<article class="article-fw">
							  <div class="inner">
								<figure>
									<a href="/read_article/{{$featuredArticle->id}}">
									  <img src="{{asset('storage/article_images/'.$featuredArticle->image)}}">
									</a>
								</figure>
								<div class="details">
								  <h1><a href="/read_article/{{$featuredArticle->id}}">
									{{$featuredArticle->title}}</a></h1>
								  {{-- <p>
									{!!Util::truncateStrings($featuredArticle->article_text)!!}  
								  </p> --}}
								  <div class="detail">
									<div class="time">{{\Carbon\Carbon::parse($featuredArticle->created_at)->format('d F Y')}}</div>
									<div class="category"><a href="/category/{{$featuredArticle->category_slug}}">{{$featuredArticle->name}}</a></div>
								  </div>
								</div>
							  </div>
							</article>
							<div class="line"></div>
								@if(count($articles)>0)
								@foreach ($latestNewsArticles as $article)
									<article class="article-mini">
									<div class="inner">
									<figure>
										<a href="/{{$article->category_slug}}/{{$article->article_slug}}">
											<img src="{{asset('storage/article_images/'.$article->image)}}">
										</a>
									</figure>
									<div class="padding">
										<h1><a href="/{{$article->category_slug}}/{{$article->article_slug}}">
										   {{$article->title}}</a></h1>
										<div class="detail">
										<div class="category"><a href="/category/{{$article->category_slug}}">{{$article->name}}</a></div>
										<div class="time">{{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</div>
										</div>
									</div>
									</div>
									</article>
								@endforeach
								@endif
						   
						  </div>
						</aside>
						<aside>
						  <div class="aside-body">
							<form class="newsletter">
                                <div class="icon">
                                    <i class="ion-ios-email-outline"></i>
                                    <h1>News Alert</h1>
                                </div>
                                <p>
                                    Send <span style="font-weight: bolder;color:red;">IKONEWS</span>
                                    to <span style="font-weight: bolder;color:red;">22384</span>
                                  </p>
                                  <p>By subscribing you will receive news alert on your Phone.</p>
                            </form>
						  </div>
						</aside>
					  </div>
					<!--  -->
					<div class="col-md-8">
						<ol class="breadcrumb">
						 <!--  <li><a href="#">Home</a></li>
						  <li class="active">Film</li> -->
						</ol>
						
						{{-- social media links --}}
						@php
						$twitter = "iko_news";
						$links="https://ikonews.co.ke/".$article->category_slug."/".$article->article_slug;
						$title = $articleItem->title;
						
						// define links
						$links = array(
							'facebook' => 'https://www.facebook.com/sharer/sharer.php?u=' . $links,
							'twitter'  => 'https://twitter.com/intent/tweet?text='. $title .'&url='. $links .'&via='. $twitter,
							'whatsapp'=>'https://wa.me/?text='.$links
						);
							
						@endphp
						<article class="article main-article">
							<header>
								<div style="font-size: calc(1.55em + 1vmin)">{{$articleItem->title}}</div>
								<ul class="details">
									<li>{{\Carbon\Carbon::parse($articleItem->created_at)->format('d F Y')}}</li>
									<li><a href="/category/{{$articleItem->category_slug}}">{{$articleItem->name}}</a></li>
									<li>By <a href="#">{{$articleItem->author}}</a></li>
								</ul>
								<br>
								<br>
								<div>
									<p>Share this article:  </p>
									<a href="{{$links['facebook']}}" class="share__link"><span class="fa fa-facebook"></span><span class="is-hidden"></span></a>
			
								<a href="{{$links['twitter']}}" class="share__link"><span class="fa fa-twitter"></span><span class="is-hidden"></span></a>
			
								<a href="{{$links['whatsapp']}}" class="share__link" data-action="share/whatsapp/share"><span class="fa fa-whatsapp"></span><span class="is-hidden"></span></a>
								</div>
							</header>
							<div class="main">
								<div class="featured">
									<figure>
										<img src="{{asset('storage/article_images/'.$articleItem->image)}}" height="300px">
										<figcaption>{{$articleItem->image_caption}}</figcaption>
									</figure>
								</div>
								<div>
								<p>
									
                                    {!!$articleItem->article_text!!}
								</p>
								</div>
							</div>
							
						</article>

						
						{{-- <div>
							<p>Share this article  </p>
							<a href="{{$links['facebook']}}" class="share__link"><span class="fa fa-facebook"></span><span class="is-hidden"></span></a>
	
						<a href="{{$links['twitter']}}" class="share__link"><span class="fa fa-twitter"></span><span class="is-hidden"></span></a>
	
						<a href="{{$links['whatsapp']}}" class="share__link" data-action="share/whatsapp/share"><span class="fa fa-whatsapp"></span><span class="is-hidden"></span></a>
						</div> --}}
					
						<div class="line thin"></div>
						
					</div>
				</div>
			</div>
		</section>

        @include('includes.website_footer')