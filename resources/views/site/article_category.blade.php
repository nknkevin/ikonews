@include('includes.website_header')

<body>	
    @include('includes.website_nav') 
    <section class="category">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-left">
            <div class="row">
              <div class="col-md-12">        
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li class="active">{{$category}}</li>
                </ol>
                <h1 class="page-title">{{$category}}</h1>
                {{-- <p class="page-subtitle">Showing all news with category <i>{{$category->name}}</i></p> --}}
              </div>
            </div>
            <div class="line"></div>
            <div class="row">
                @if(count($articles)>0)
                    @foreach ($articles as $article)
                        <article class="col-md-12 article-list">
                            <div class="inner">
                            <figure>
                                <a href="/{{$article->category_slug}}/{{$article->article_slug}}">
                                    <img src="{{asset('storage/article_images/'.$article->image)}}">
                                </a>
                            </figure>
                            <div class="details">
                                <div class="detail">
                                <div class="category">
                                <a href="/category/{{$article->category_slug}}/">{{$article->name}}</a>
                                </div>
                                <div class="time">{{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</div>
                                </div>
                                <h1><a href="/{{$article->category_slug}}/{{$article->article_slug}}">{{$article->title}}</a></h1>
                                <p>
                                    {!!Util::truncateStrings($article->article_text)!!}  
                                </p>
                                <footer>
                                {{-- <a href="#" class="love"><i class="ion-android-favorite-outline"></i> <div>237</div></a> --}}
                                <a class="btn btn-primary more" href="/{{$article->category_slug}}/{{$article->article_slug}}">
                                    <div>More</div>
                                    <div><i class="ion-ios-arrow-thin-right"></i></div>
                                </a>
                                </footer>
                            </div>
                            </div>
                        </article>
                    @endforeach
                    @endif
             
              
              <div class="col-md-12 text-center">
               
                <div class="pagination-help-text">
                    @if(count($articles)>1)
                    {{$articles->links()}}
                    @endif
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 sidebar">
            <aside>
              <div class="aside-body">
                <figure class="ads">
                    <a href="https://kazilinks.com">
                      <img src="{{asset('website/images/ad.png')}}">
                    </a>
                  <figcaption>Advertisement</figcaption>
                </figure>
              </div>
            </aside>
            <aside>
              <h1 class="aside-title">Latest News</h1>
              <div class="aside-body">
                <article class="article-fw">
                  <div class="inner">
                    <figure>
                        <a href="/{{$article->category_slug}}/{{$featuredArticle->slug}}">
                          <img src="{{asset('storage/article_images/'.$featuredArticle->image)}}">
                        </a>
                    </figure>
                    <div class="details">
                      <h1><a href="/{{$article->category_slug}}/{{$featuredArticle->slug}}">
                        {{$featuredArticle->title}}</a></h1>
                      <p>
                        {!!Util::truncateStrings($featuredArticle->article_text)!!}  
                      </p>
                      <div class="detail">
                        <div class="time">{{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</div>
                        <div class="category"><a href="/category/{{$article->category_slug}}">{{$article->name}}</a></div>
                      </div>
                    </div>
                  </div>
                </article>
                
                <div class="line"></div>
                <h1 class="aside-title">Recent News</h1>
                    @if(count($articles)>0)
                    @foreach ($latestNewsArticles as $article)
                        <article class="article-mini">
                        <div class="inner">
                        <figure>
                            <a href="/{{$article->category_slug}}/{{$article->article_slug}}">
                                <img src="{{asset('storage/article_images/'.$article->image)}}">
                            </a>
                        </figure>
                        <div class="padding">
                            <h1><a href="/{{$article->category_slug}}/{{$article->article_slug}}">
                               {{$article->title}}</a></h1>
                            <div class="detail">
                            <div class="category"><a href="/category/{{$article->category_slug}}">{{$article->name}}</a></div>
                            <div class="time">{{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</div>
                            </div>
                        </div>
                        </div>
                        </article>
                    @endforeach
                    @endif
               
              </div>
            </aside>
            <aside>
              <div class="aside-body">
                <form class="newsletter">
                  <div class="icon">
                      <i class="ion-ios-email-outline"></i>
                      <h1>News Alert</h1>
                  </div>
                  <p>
                      Send <span style="font-weight: bolder;color:red;">IKONEWS</span>
                      to <span style="font-weight: bolder;color:red;">22384</span>
                    </p>
                    <p>By subscribing you will receive news alert on your Phone.</p>
              </form>
              </div>
            </aside>
          </div>
          
        </div>
      </div>
    </section>

@include('includes.website_footer')