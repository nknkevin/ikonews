
  @include('includes.header')

@include('layouts.sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SMS Alerts
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">IkoNews</a></li>
      <li class="active">New SMS Alert </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >New SMS Alert</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <button class="btn btn-info pull-left" onclick="location.href = '/sms_alert_list';">
                  Alerts List</button>
          
                  <div class="card">

                    
            
                    <div class="card-body">
                        <form action="/api/smsalerts" class="d-block ajaxForm" method="POST" enctype="multipart/form-data">
                            @csrf
                            {{-- <input type="hidden" name="user_id" value={{Auth::user()->id}}> --}}
                            <div class="form-group col-md-12">
                                <label for="title">Title * </label>
                                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($facility) ? $facility->title : '') }}" required>
                                <span id="title-error" class="help-block"></span>
                                <small id="title_help" class="form-text text-muted">Provide Title</small>
                            </div>

                            <div class="form-group col-md-12">
                              <label for="link">Article Link * </label>
                              <input type="text" id="link" name="link" class="form-control" value="{{ old('title', isset($facility) ? $facility->title : '') }}" required>
                              <span id="title-error" class="help-block"></span>
                              <small id="title_help" class="form-text text-muted">Provide Article Link</small>
                          </div>
            
                            <input type="hidden" name="category_id" value="1">
                            {{-- <div class="form-group col-md-12">
                                <label for="category_id"> Category *</label>
                                <select name="category_id" class="form-control" required>
                                    <option value=""> Select Category</option>
                                         @foreach($categories as $row)
                                         <option value="{{ $row->id }}">{{ $row->name }}</option>
                                        @endforeach
                                        </select>
                                <span id="category-id-error" class="help-block"></span>
                                <small id="category_id_help" class="form-text text-muted">Provide Category</small>
                             </div> --}}
            
            
                            <div class="form-group col-md-12">
                                <label for="code">Description *  </label>
                                <textarea name="description" id="description" class="form-control char-textarea" rows="4" cols="80" onKeyDown="limitText(this.value);"
                                onKeyUp="limitText(this.value);"  maxlength="136" required></textarea> 
                                <span id="description-error" class="help-block"></span>
                                <small id="charactersRemaining" class="form-text text-muted font-weight-bold char-count ">Maximum Characters: 136</small>
                            </div>
            
            
                            <div class="col-md-3">
                                <button class="btn btn-block btn-primary" id="saveBtn" type="submit">Create Alert</button>
                            </div>
                            
                        </form>
                
                
                    </div>
                </div>


          </div>
          <!-- /.box-body -->
         
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

{{--  --}}

<script>
  function limitText(text) {
      $('.fn_countdown').text('Remaining Characters:' + (137 - text.length));
 }

 $(".char-textarea").on("keyup",function(event){
  checkTextAreaMaxLength(this,event);
});

/*
Checks the MaxLength of the Textarea
-----------------------------------------------------
@prerequisite:	textBox = textarea dom element
				e = textarea event
                length = Max length of characters
*/
function checkTextAreaMaxLength(textBox, e) { 
    
    var maxLength = parseInt($(textBox).data("length"));
    
  
    if (!checkSpecialKeys(e)) { 
        if (textBox.value.length > maxLength - 1) textBox.value = textBox.value.substring(0, maxLength); 
   } 
  $(".char-count").html(maxLength - textBox.value.length);
    
    return true; 
} 
/*
Checks if the keyCode pressed is inside special chars
-------------------------------------------------------
@prerequisite:	e = e.keyCode object for the key pressed
*/
function checkSpecialKeys(e) { 
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) 
        return false; 
    else 
        return true; 
}

 
 </script>



@include('includes.footer')
