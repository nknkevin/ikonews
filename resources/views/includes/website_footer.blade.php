<!-- Start footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="block">
          <h1 class="block-title">Company Info</h1>
          <div class="block-body">
            <figure class="foot-logo">
              <img src="{{asset('website/images/logo_light.png')}}" class="img-responsive" alt="Logo">
            </figure>
            <p class="brand-description">
              IKONews Fresh and Precise.
            </p>
            <a href="#" class="btn btn-Iko News white">About Us <i class="ion-ios-arrow-thin-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        
        
        <div class="block">
          <h1 class="block-title">News Alert</h1>
          <div class="block-body">
            <form class="newsletter">
              {{-- <div class="icon">
                  <i class="ion-ios-email-outline"></i>
                  <h1>News Alert</h1>
              </div> --}}
              <p>
                  Send <span style="font-weight: bolder;color:red;">IKONEWS</span>
                  to <span style="font-weight: bolder;color:red;">22384</span>
                </p>
                <p>By subscribing you will receive news alert on your Phone.</p>
          </form>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="block">
          <h1 class="block-title">Latest News</h1>
          <div class="block-body">
            {{-- <article class="article-mini">
              <div class="inner">
                <figure>
                  <a href="read_article/{{$featuredArticle->id}}">
                    <img src="{{asset('website/images/news/img12.jpg')}}" alt="Sample Article">
                  </a>
                </figure>
                <div class="padding">
                  <h1><a href="/">{{$featuredArticle->title}}</a></h1>
                </div>
              </div>
            </article> --}}
            
            <a href="#" class="btn btn-Iko News white btn-block">See All <i class="ion-ios-arrow-thin-right"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-xs-12 col-sm-6">
        <div class="block">
          <h1 class="block-title">Follow Us</h1>
          <div class="block-body">
            <p>Follow us and stay in touch to get the latest news</p>
            <ul class="social trp">
              <li>
                <a href="https://www.facebook.com/Ikonews.co.ke/" class="facebook">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-facebook"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/iko_news" class="twitter">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-twitter-outline"></i>
                </a>
              </li>
        
              <li>
                <a href="https://youtube.com/playlist?list=UUH_Us8rDWzDlT3phdzUcDKg" class="youtube">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-youtube"></i>
                </a>
              </li>
              
            </ul>
          </div>
        </div>
        <div class="line"></div>
        <div class="block">
          <div class="block-body no-margin">
            <ul class="footer-nav-horizontal">
              <li><a href="/">Home</a></li>
              <li><a href="#">Partners</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">About</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="copyright">
          COPYRIGHT &copy; Iko News 2021. ALL RIGHT RESERVED.
          <div>
          
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- End Footer -->

<!-- JS -->
<!-- JS -->
<script src="../website/js/jquery.js"></script>
<script src="../website/js/jquery.migrate.js"></script>
<script src="../website/scripts/bootstrap/bootstrap.min.js"></script>
<script>var $target_end=$(".best-of-the-week");</script>
<script src="../website/scripts/jquery-number/jquery.number.min.js"></script>
<script src="../website/scripts/owlcarousel/dist/owl.carousel.min.js"></script>
<script src="../website/scripts/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
<script src="../website/scripts/easescroll/jquery.easeScroll.js"></script>
<script src="../website/scripts/sweetalert/dist/sweetalert.min.js"></script>
<script src="../website/scripts/toast/jquery.toast.min.js"></script>
<script src="../website/scripts/textroller/index.js"></script>
<script src="../website/scripts/textroller/text-roller.js"></script>
<!-- <script src="js/demo.js"></script> -->
<script src="../website/js/e-magz.js"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.2.7/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.2.7/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyCNdGckff6VFEGpCozwJ9ceq1UZuw6xJhw",
    authDomain: "ikonews.firebaseapp.com",
    projectId: "ikonews",
    storageBucket: "ikonews.appspot.com",
    messagingSenderId: "830481196770",
    appId: "1:830481196770:web:9016bb9f39606bdef6a0bb",
    measurementId: "G-JEHSJEN7XS"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
</script>
</body>
</html>

  {{-- {{asset('dist/css/AdminLTE.min.css')}} --}}