<header class="primary">
  <div class="firstbar">
    <div class="container" style="background-color: #ffffff">
      {{-- <div class="brand visible-sm-block, hidden-sm">
        <a href="/">
          <img src="{{asset('website/images/logo.png')}}" alt="Iko News Logo">
        </a>
      </div> --}}
      <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="brand">
            <div style="padding: 5px;">
              {{-- <h2 style="color: red;">Iko News</h2> --}}
              <img src="{{asset('website/images/logo.png')}}" height="80px" alt="Iko News Logo"><br>
            </div>
          </div>
        </div>

        <div class="col-md-7 col-sm-6 visible-lg-block visible-md-block">

          <style type="text/css">
            @keyframes shake {
              0% { transform: translate(1px, 1px) rotate(0deg); }
              10% { transform: translate(-1px, -2px) rotate(-1deg); }
              20% { transform: translate(-3px, 0px) rotate(1deg); }
              30% { transform: translate(3px, 2px) rotate(0deg); }
              40% { transform: translate(1px, -1px) rotate(1deg); }
              50% { transform: translate(-1px, 2px) rotate(-1deg); }
              60% { transform: translate(-3px, 1px) rotate(0deg); }
              70% { transform: translate(3px, 1px) rotate(-1deg); }
              80% { transform: translate(-1px, -1px) rotate(1deg); }
              90% { transform: translate(1px, 2px) rotate(0deg); }
              100% { transform: translate(1px, -2px) rotate(-1deg); }
        }
        #topAd {
            animation: shake 20s;
            animation-iteration-count: infinite;
        }
            </style>
          
            <p style="padding: 5px; margin-top:15px;">
              {{-- <h2 style="color: red;">Iko News</h2> --}}
              {{-- <img id="topAd" class="visible-lg-block visible-md-block" src="{{asset('website/images/t.png')}}" height="80px" width="100%" alt="Iko News Logo"><br> --}}
            </p>
         
        </div>
        <div class="col-md-2 col-sm-3 visible-lg-block visible-md-block" style="margin-top: 15px;">
          <p>Follow us on social media</p>
            <ul class="social trp">
              <li>
                <a href="https://www.facebook.com/Ikonews.co.ke/" class="facebook">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-facebook"></i>
                </a>
              </li>
              <li>
                <a href="https://twitter.com/iko_news" class="twitter">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-twitter-outline"></i>
                </a>
              </li>
        
              <li>
                <a href="https://youtube.com/playlist?list=UUH_Us8rDWzDlT3phdzUcDKg" class="youtube">
                  <svg><rect width="0" height="0"/></svg>
                  <i class="ion-social-youtube"></i>
                </a>
              </li>
            </ul>
          </div>
        {{-- <div class="col-md-3 col-sm-8">
          <div class="banner" style="margin-top: 5px; background-color:red;">
            <a href="#">
                <img src="{{asset('website/images/ads.png')}}"  alt="Advertisement">
            </a>
        </div>
        </div> --}}
      
      </div>
   
      
      </div>
    </div>
  </div>

  <!-- Start nav -->
  <nav class="menu">
    <div class="container">
      <div class="brand">
        <a href="/">
          <img src="{{asset('website/images/logo.png')}}" alt="Iko News Logo">
        </a>
      </div>
      <div class="mobile-toggle">
        <a href="#" data-toggle="menu" data-target="#menu-list"><i class="ion-navicon-round"></i></a>
      </div>
      <div class="mobile-toggle">
        <a href="#" data-toggle="sidebar" data-target="#sidebar"><i class="ion-ios-arrow-left"></i></a>
      </div>
      <div id="menu-list">
        <ul class="nav-list">
          <li class="for-tablet nav-title"><a>Menu</a></li>
          <li class="for-tablet"><a href="login.html">Login</a></li>
          <li class="for-tablet"><a href="register.html">Register</a></li>
          <li><a href="/">Home</a></li>
          <li><a href="/">Latest</a></li>
          <li><a href="/article_category/7">Business</a></li>
          <li><a href="/article_category/8">Power Play</a></li>
          {{-- <li class="dropdown magz-dropdown"><a href="#">Regions</a>
            <ul class="dropdown-menu">
            <li><a href="#"><i ></i> Coastal Region</a></li>
            <li><a href="#"><i></i> Central Region</a></li>
            <li><a href="#"><i></i> RiftValley Region</a></li>
            <li><a href="#"><i></i> Lake Region</a></li>
            <li><a href="#"><i></i> Northern  Region</a></li>
            </ul>
          </li> --}}
          <li><a href="/article_category/3">Opinions</a></li>
          <li><a href="/article_category/6">International</a></li>
          <li><a href="/article_category/5">Sports</a></li>
          
          
          <li class="dropdown magz-dropdown"><a href="#">More <i></i></a>
            <ul class="dropdown-menu">
              {{-- <li><a href="#"><i class="icon ion-person"></i> My Account</a></li>
              <li><a href="#"><i class="icon ion-settings"></i> Settings</a></li> --}}
            
            </ul>
          </li>
          <li></li>
          <li></li>
          <li></li>
          <li class="dropdown magz-dropdown"><a href="#"><i class="icon ion-person"></i>Subscribe </a>
            <ul class="dropdown-menu">
              <li><a href="/login"><i class="icon ion-settings"></i> Login</a></li>
              <li><a href="/userregistration"><i class="icon ion-log-out"></i> Register</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '483819653031078'); 
  fbq('track', 'PageView');
  </script>
  <noscript>
   <img height="1" width="1" 
  src="https://www.facebook.com/tr?id=483819653031078&ev=PageView
  &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->
  <!-- End nav -->
</header>
