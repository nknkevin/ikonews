<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="Iko News .">
		<meta name="author" content="Iko News">
		<meta name="keyword" content="Iko News, Fresh and Precise">
		<!-- Shareable -->
		<meta property="og:title" content="Iko News .Fresh and Precise" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="https://ikonews.co.ke/" />
		<meta property="og:image" content="https://ikonews.co.ke/website/images/logo.png" />
		<title>Iko News. Fresh and Precise</title>
		
		<link rel="shortcut icon" href="{{asset('website/images/favicon.ico')}}" type="image/x-icon">
		<link rel="icon" href="{{asset('website/images/favicon.ico')}}" type="image/x-icon">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{asset('website/scripts/bootstrap/bootstrap.min.css')}}">
		<!-- IonIcons -->
		<link rel="stylesheet" href="{{asset('website/scripts/ionicons/css/ionicons.min.css')}}">
		<!-- Toast -->
		<link rel="stylesheet" href="{{asset('website/scripts/toast/jquery.toast.min.css')}}">
		<!-- OwlCarousel -->
		<link rel="stylesheet" href="{{asset('website/scripts/owlcarousel/dist/assets/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('website/scripts/owlcarousel/dist/assets/owl.theme.default.min.css')}}">
		<!-- Magnific Popup -->
		<link rel="stylesheet" href="{{asset('website/scripts/magnific-popup/dist/magnific-popup.css')}}">
		<link rel="stylesheet" href="{{asset('website/scripts/sweetalert/dist/sweetalert.css')}}">
		<!-- Custom style -->
		<link rel="stylesheet" href="{{asset('website/css/style.css')}}">
		<link rel="stylesheet" href="{{asset('website/css/skins/all.css')}}">
		<link rel="stylesheet" href="{{asset('website/css/demo.css')}}">

		<script data-ad-client="ca-pub-7986749887839167" 
		async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window,document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			 fbq('init', '483819653031078'); 
			fbq('track', 'PageView');
			</script>
			<noscript>
			 <img height="1" width="1" 
			src="https://www.facebook.com/tr?id=483819653031078&ev=PageView
			&noscript=1"/>
			</noscript>
			<!-- End Facebook Pixel Code -->
			<!-- End nav -->
  </head>
