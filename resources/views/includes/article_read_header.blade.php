<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="Iko News .">
		<meta name="author" content="Iko News">
		<meta name="keyword" content="Iko News, Fresh and Precise">
		<!-- Shareable -->
		<meta property="og:title" content="{{$articleItem->title}}" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="{{$articleItem->category_slug}}/{{$articleItem->article_slug}}" />
		<meta property="og:image" content="{{asset('storage/article_images/'.$articleItem->image)}}" />
		<title>Iko News. Fresh and Precise</title>
		
		<link rel="shortcut icon" href="{{asset('website/images/favicon.ico')}}" type="image/x-icon">
		<link rel="icon" href="{{asset('website/images/favicon.ico')}}" type="image/x-icon">
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{asset('website/scripts/bootstrap/bootstrap.min.css')}}">
		<!-- IonIcons -->
		<link rel="stylesheet" href="{{asset('website/scripts/ionicons/css/ionicons.min.css')}}">
		<!-- Toast -->
		<link rel="stylesheet" href="{{asset('website/scripts/toast/jquery.toast.min.css')}}">
		<!-- OwlCarousel -->
		<link rel="stylesheet" href="{{asset('website/scripts/owlcarousel/dist/assets/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('website/scripts/owlcarousel/dist/assets/owl.theme.default.min.css')}}">
		<!-- Magnific Popup -->
		<link rel="stylesheet" href="{{asset('website/scripts/magnific-popup/dist/magnific-popup.css')}}">
		<link rel="stylesheet" href="{{asset('website/scripts/sweetalert/dist/sweetalert.css')}}">
		<!-- Custom style -->
		<link rel="stylesheet" href="{{asset('website/css/style.css')}}">
		<link rel="stylesheet" href="{{asset('website/css/skins/all.css')}}">
		<link rel="stylesheet" href="{{asset('website/css/demo.css')}}">
		{{-- social icons share --}}
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style type="text/css">
			/* Style all font awesome icons */
			.fa {
			  padding: 10px;
			  font-size: 20px;
			  width: 40px;
			  text-align: center;
			  text-decoration: none;
			}
	
			/* Add a hover effect if you want */
			.fa:hover {
			  opacity: 0.7;
			}
	
			/* Set a specific color for each brand */
	
			/* Facebook */
			.fa-facebook {
			  background: #3B5998;
			  color: white;
			}
	
			/* Twitter */
			.fa-twitter {
			  background: #55ACEE;
			  color: white;
			}
	
			/* Twitter */
			.fa-whatsapp {
			  background: #25D366;
			  color: white;
			}
		</style>
  </head>
