
  @include('includes.header')

  @include('layouts.sidebar')

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Article
            <small>Edit Article</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="productscategory">Ikonews</a></li>
            <li class="active">Edit Article </li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Edit {{$article->title}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

            {!! Form::open(['action' => ['ArticleController@update',$article->id],'method'=>'POST','enctype'=>'multipart/form-data'])!!}
 
            <div class="form-group">
            
              {{Form::label('title', 'Article Title')}}
              {{Form::text('title', $article->title,['class'=>'form-control','placeholder'=>'Article Title'])}}
            </div>
            <div class="form-group">
            
              {{Form::label('slug', 'Article Alias')}}
              {{Form::text('slug', $article->slug,['class'=>'form-control','placeholder'=>'Article Alias'])}}
            </div>

            <div class="form-group">
            
              {{Form::label('author', 'Article author')}}
              {{Form::text('author', $article->author,['class'=>'form-control','placeholder'=>'Article Author'])}}
            </div>
              <div class="form-group">
                {!! Form::label('category_id', 'Category:') !!}
                @php
                  $options = $categories->pluck('name','id')->toArray()
                @endphp
                {!! Form::select('category_id', $options,null, ['class' => 'form-control', 'placeholder' => 'Select Category']) !!}
              </div>
                <div class="form-group">
                  {{Form::label('image', 'Article Image')}}
                  {{Form::file('image',array('onchange' => 'loadFile(event)') )}}
                  <img id="output" src="{{asset('storage/article_images/'.$article->image)}}" width="150dp"/><br>
                  {{Form::label('image_caption', 'Article Image Caption')}}
                  {{Form::text('image_caption', $article->image_caption,['class'=>'form-control','placeholder'=>'Caption'])}}
             
                </div>

                <div class="form-group">
                 

              {{Form::label('article_text', 'Body')}}
              {{Form::textarea('article_text', $article->article_text,['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Article'])}}
            </div>
          
              <div class="form-group">
              {{Form::hidden('category_id', $article->category_id,['class'=>'form-control','hidden'])}}
              {{Form::hidden('_method', 'PUT')}}
              {{Form::submit('Update',['class'=>'btn btn-primary pull-left'])}}
            </div>
            
            {!! Form::close() !!} 
  
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <script>
        var loadFile = function(event) {
          var output = document.getElementById('output');
          output.src = URL.createObjectURL(event.target.files[0]);
          output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
          }
        };
      </script>

  @include('includes.footer')