
@include('includes.header')

@include('layouts.editor_sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Editor Dashboard
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">IkoNews</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >Home</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
              
    
                <div class="row">
                      <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-purple">
                          <div class="inner">
                          <h3>{{count($pending_articles)}}</h3>
              
                          <p>Pending Articles</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-bookmarks-outline"></i>
                          </div>
                          <a href="/editorArticles/pending" class="small-box-footer">
                            View Pending Articles <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                      </div>   
                     
                      <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-blue">
                          <div class="inner">
                          <h3>{{count($published_articles)}}</h3>
                          <p>Published Articles</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-book"></i>
                          </div>
                          <a href="/editorArticles/published" class="small-box-footer">
                            View Published <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                      </div>
                   
                </div>
          
        

          <div class="row">
                      <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-orange">
                          <div class="inner">
                          <h3>0</h3>
              
                          <p>Reporters</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-bookmarks-outline"></i>
                          </div>
                          <a href="#" class="small-box-footer">
                            View All Reporters <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                      </div>   
                     
                    
                      <div class="col-lg-6 col-xs-6">
                        <!-- small box -->
                        <div class="small-box bg-green">
                          <div class="inner">
                          <h3>{{count($categories)}}</h3>
                          <p> Articles Categories</p>
                          </div>
                          <div class="icon">
                            <i class="ion ion-book"></i>
                          </div>
                          <a href="categoryList" class="small-box-footer">
                            View All Article Categories <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                      </div>
                   
                </div>
          
          </div>
          <!-- /.box-body -->
        
        </div>
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->


@include('includes.footer')
