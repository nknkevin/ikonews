
  @include('includes.header')

@include('layouts.sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Articles
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">IkoNews</a></li>
      <li class="active">All Articles</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >All Pending Articles</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          {{-- <button class="btn btn-info pull-left" onclick="location.href = '/newArticle';">
                  Add New Article</button> --}}
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th></th>
                <th>Article Title</th>
                <th></th>
              
              </tr>
              </thead>
              <tbody>

              @if(count($data)>0)
                @foreach ($data as $_data)

                    <tr>
                      <td></td>
                      <td>
                        <img id="output" src="{{asset('storage/article_images/'.$_data->image)}}" width="80dp"/>
                      </td>
                      <td><h4>{{$_data->title}}</h4>
                        <br>
                       <i><b>Author: </b>{{$_data->author}} - Created at: {{$_data->created_at}}</i>
                        <a href="/editorArticle/pending/{{$_data->id}}" class=" pull-right"
                          style="margin-right: 20px;">View</a>
                      </td>


                      <td></td> 
                    {{-- <td> <button class="btn btn-info pull-left" onclick="location.href = '/article/{{$_data->id}}';">
                        View</button></td> --}}
                      </tr>

                @endforeach
              @endif
          
              </tbody>
              <tfoot>
              
              </tfoot>
            </table>


          </div>
          <!-- /.box-body -->
          @if(count($data)>1)
          {{$data->links()}}
         @endif
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

{{--  --}}

@include('includes.footer')
