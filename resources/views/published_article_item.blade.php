
  @include('includes.header')

@include('layouts.sidebar')

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Articles
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">IkoNews</a></li>
      <li class="active">{{$article->title}}</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title text-primary" >Published Article</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <button class="btn btn-info pull-left" onclick="location.href = '/reporterArticles/published';">
                  Article List</button>
                  {{-- <button class="btn btn-secondary pull-right" onclick="location.href = '/editArticle/{{$article->id}}';">
                    Edit Article</button> --}}
                  <hr>
            
            <h3>{{$article->title}}</h3>
            <p> <i><b>Author: </b>{{$article->author}} - Created at: {{\Carbon\Carbon::parse($article->created_at)->format('d F Y')}}</i></p>
            <img src="{{asset('storage/article_images/'.$article->image)}}" height="200px">
            <p style="font-style: italic">{{$article->image_caption}}</p>
            <p>
              {!!$article->article_text!!}
            </p>

          </div>
        
        </div>
     
            <!-- /.box-body -->
          </div>
        <!-- /.box -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <!-- /.row -->
  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

{{--  --}}

@include('includes.footer')
