
  @include('includes.header')

  @include('layouts.sidebar')

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Category
            <small>Edit Category</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="productscategory">Category</a></li>
            <li class="active">Edit Category </li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Edit {{$category->name}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

            {!! Form::open(['action' => ['ArticleCategoryController@update',$category->id],'method'=>'POST'])!!}
 
            <div class="form-group">
              {{Form::label('name', 'Name')}}
              {{Form::text('name', $category->name,['class'=>'form-control','placeholder'=>'Category Name'])}}
            </div>

            <div class="form-group">
              {{Form::label('slug', 'Alias')}}
              {{Form::text('slug', $category->slug,['class'=>'form-control','placeholder'=>'Category Link Alias'])}}
            </div>
          
              <div class="form-group">
                {{Form::hidden('_method', 'PUT')}}
              {{Form::submit('Save',['class'=>'btn btn-info pull-left'])}}
            </div>
            
            {!! Form::close() !!} 
            
  
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->


  @include('includes.footer')