<?php

namespace App\Jobs;

use Facebook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class FacebookPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $link;
    protected $articleImage;
    protected $status;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($articleImageFile, $status)
    {
        $this->status = $status;
        $this->articleImage = $articleImageFile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //auto initiate posting an article to facebbok
        //$this->sendFbPost($this->articleImage,$this->status);
    }

    public function sendFbPost($articleImage, $status)
    {
        $fb = new Facebook\Facebook([
            'app_id' => '811366672923984',
            'app_secret' => '83c5c3cad0da2c986216047ca5cb54fa',
            'default_graph_version' => 'v10.0',

            ]);

        $path = Storage::disk('public')->path('article_images/'.$articleImage);

        $linkData = [
              'message' => $status,
              'source' => $fb->fileToUpload($path),
            ];

        // $linkData = [
        //     'message' => 'Three  charged with attempt to con credit firm Sh4.6m Read more: https://ikonews.co.ke/read_article/752',
        //     'url' => 'https://pbs.twimg.com/media/EzZ6zJEWEAQFcko?format=jpg&name=medium',
        //   ];

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->post('/me/photos', $linkData, 'EAALh7wZASfVABAIclJNGllNi1yRKmzZC3YRXw687kA6yZBSBIgZCahGx49dvZBoF4X0zgjm8azZCLDYJRUzx5jKi1mAhP88OMBqi38e8CjDkWy6Wqji4NZCGOyaXkDFg3o5BucB3e0jPxDdCKeCzkWSxuZBcIZBCvcCb221k6QMG1fwZDZD');
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: '.$e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: '.$e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

        echo 'Posted with id: '.$graphNode['id'];
    }
}
