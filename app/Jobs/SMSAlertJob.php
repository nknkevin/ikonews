<?php

namespace App\Jobs;

use App\Models\SmsAlert;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SMSAlertJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //get unsend messages
        $data = SmsAlert::where('send_status', 0)->get();
        foreach ($data as $item) {
            //
            $data = [
            'title' => $item->title,
            'description' =>$item->description,
            'category_id' =>$item->category_id,
            'keyword_id'=>1,
            'active' =>1, ];

            //$this->sendSMSAlert($data,$item->id);
        }
    }

    public function sendSMSAlert($data, $id)
    {
        $baseUrl = 'https://subscription.ikonews.co.ke/api/post_news_alert';
        $ch = curl_init($baseUrl);
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json', 'Accept:application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        echo 'Result '.$result;
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode == 200) {
            $this->updateStatus($id);
        }
        curl_close($ch);
    }

    //update status
    public function updateStatus($id)
    {
        $smsAlert = SmsAlert::find($id);
        $smsAlert->send_status = 1;
        $smsAlert->save();
    }
}
