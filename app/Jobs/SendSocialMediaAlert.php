<?php

namespace App\Jobs;

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class SendSocialMediaAlert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $status;

    protected $articleImage;
    protected $link;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($articleImageFile, $status)
    {
        //
        $this->status = $status;
        $this->articleImage = $articleImageFile;
        // $this->link = $link;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        //$this->sendTweet($this->articleImage,$this->status);
    }

    //send tweets
    public function sendTweet($articleImage, $status)
    {
        define('CONSUMER_KEY', 'dhT308AZxEMQ7FWNc801Rt13M');
        define('CONSUMER_SECRET', 'PAwlR3CabyMd6JCCCUsv7cpBjeOmAV30BmKhNrsSyqVbu7W2YU');
        define('ACCESS_TOKEN', '1353594887799795713-vJeYumVQaVXaYrmdHsRd1W9ovKbVxE');
        define('ACCESS_TOKEN_SECRET', 'KIdKWvyjS0iaPlih6SwqYYFoKNTPAETyStV8vTXGsi9Uc');

        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

        $path = Storage::disk('public')->path('article_images/'.$articleImage);

        //return serialize($path);
        $media1 = $connection->upload('media/upload', ['media' =>$path]);
        $parameters = [
            'status' => $status,
            'media_ids' => implode(',', [$media1->media_id_string]),
        ];
        $result = $connection->post('statuses/update', $parameters);
    }
}
