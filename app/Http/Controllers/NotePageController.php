<?php

namespace App\Http\Controllers;

use App\Models\NotePage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NotePage::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'page_no'=>'required',
            'text'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $note = new NotePage();
        $note->topic_id = $request->input('topic_id');
        $note->page_no = $request->input('page_no');
        $note->text = $request->input('text');
        $note->save();

        return redirect('/allNotes/'.$request->input('topic_id'))->with('success', 'Note Topic added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return NotePage::where('topic_id', '=', $id)->orderBy('page_no')->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'page_no'=>'required',
            'text'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $note = NotePage::find($id);
        $note->topic_id = $request->input('topic_id');
        $note->page_no = $request->input('page_no');
        $note->text = $request->input('text');
        $note->save();

        return redirect('/allNotes/'.$request->input('topic_id'))->with('success', 'Notes Editted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notepage = NotePage::find($id);
        $topicId = $notepage->topic_id;
        $notepage->delete();

        return redirect('/allNotes/'.$topicId)->with('success', 'Note Page deleted successfully');
    }
}
