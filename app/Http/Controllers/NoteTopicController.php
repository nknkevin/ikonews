<?php

namespace App\Http\Controllers;

use App\Models\NoteTopic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NoteTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NoteTopic::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $topic = new NoteTopic();
        $topic->name = $request->input('name');
        $topic->subject_id = $request->input('subject_id');
        $topic->type = $request->input('type');
        $topic->save();

        return redirect('/allNotetopics/'.$request->input('subject_id'))->with('success', 'Note Topic added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $topic = NoteTopic::find($id);
        $topic->name = $request->input('name');
        $topic->type = $request->input('type');
        $topic->form = $request->input('form');
        $topic->subject_id = $request->input('subject_id');
        $topic->save();

        return redirect('/allNotetopics/'.$request->input('subject_id'))->with('success', 'Note Topic editted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
