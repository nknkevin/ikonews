<?php

namespace App\Http\Controllers;

use App\Models\NotePage;
use App\Models\NoteSubject;
use App\Models\NoteTopic;
use App\Models\Question;
use App\Models\Subject;
use App\Models\Topic;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function addnewquestion($topic_id)
    {
        $subject_id = Topic::where('id', '=', $topic_id)->first();
        $data = [
           'topic_id'=>$topic_id,
           'subject_name'=>Subject::where('id', '=', $subject_id->subject_id)->first(),
           'topic_name'=>Topic::where('id', '=', $topic_id)->first(),
       ];

        return view('newquestion')->with($data);
    }

    public function allquestions($topic_id)
    {
        $subject_id = Topic::where('id', '=', $topic_id)->first();
        $data = [
            'topic_id'=>$topic_id,
            'topic_name' =>Topic::where('id', '=', $topic_id)->first(),
            'subject_name'=>Subject::where('id', '=', $subject_id->subject_id)->first(),
            'data'=>Question::where('topic_id', '=', $topic_id)->orderByDesc('created_at')->paginate(20),
        ];

        return view('questions')->with($data);
    }

    //edit question
    public function editQuestion($quiz_id)
    {
        $question = Question::find($quiz_id);

        return view('editquestion')->with('question', $question);
    }

    //show subjects
    public function allSubjects()
    {
        $data = Subject::orderBy('created_at')->paginate(20);

        return view('allsubjects')->with('data', $data);
    }

    //add new subjects
    public function newsubject()
    {
        return view('addsubject');
    }

    //.....................notes............
    //new note subject
    public function newnotesubject()
    {
        return view('addnotesubject');
    }

    //new note topic
    public function newnotetopic($subject_id)
    {
        $data = [
            'subject_id'=>$subject_id,
        ];

        return view('addnotetopic')->with($data);
    }

    //edit notes topic
    public function editSubjectNoteTopic($topic_id)
    {
        $data = NoteTopic::find($topic_id);

        return view('editnotestopic')->with('data', $data);
    }

    //add note
    public function newnote($topic_id)
    {
        //$notesPage = NotePage::where("topic_id","=",$topic_id)->orderBy('created_at','desc')->first();
        $pageNo = 1;
        // if($notesPage!=null){
        //     $pageNo = ($notesPage->page_no)+1;
        // }
        $data = [
            'topic_id'=>$topic_id,
            'page_no'=>$pageNo,
        ];

        return view('addnote')->with($data);
    }

    //edit Notes
    public function editnotespage($note_id)
    {
        $notesPage = NotePage::find($note_id);

        return view('editpagenote')->with('notespage', $notesPage);
    }

    //all subjects
    public function allNoteSubject()
    {
        $data = NoteSubject::orderBy('created_at')->paginate(20);

        return view('allnotesubjects')->with('data', $data);
    }

    //all topics
    public function allNotetopics($subject_id)
    {
        $data = [
            'subject_id'=>$subject_id,
            'subject_name'=>NoteSubject::where('id', '=', $subject_id)->first(),
            'data'=>NoteTopic::where('subject_id', '=', $subject_id)->orderBy('created_at')->paginate(20),
        ];

        return view('allnotetopics')->with($data);
    }

    //all notes
    public function allNotes($topic_id)
    {
        $subject_id = NoteTopic::where('id', '=', $topic_id)->first();
        $data = [
            'topic_id'=>$topic_id,
            'topic_name' =>NoteTopic::where('id', '=', $topic_id)->first(),
            'subject_name'=>NoteSubject::where('id', '=', $subject_id->subject_id)->first(),
            'data'=>NotePage::where('topic_id', '=', $topic_id)->orderBy('created_at')->paginate(20),
        ];

        return view('notes')->with($data);
    }

    //.....................................

    //show all topics
    public function alltopics($subject_id)
    {
        $data = [
            'subject_id'=>$subject_id,
            'subject_name'=>Subject::where('id', '=', $subject_id)->first(),
            'data'=>Topic::where('subject_id', '=', $subject_id)->orderBy('created_at')->paginate(20),
        ];

        return view('alltopics')->with($data);
    }

    public function editTopic($subject_id)
    {
        $topic = Topic::find($subject_id);

        return view('edittopic')->with('topic', $topic);
    }

    //add new topics
    public function newtopic($subject_id)
    {
        $data = [
            'subject_id'=>$subject_id,
            'subject'=>Subject::where('id', '=', $subject_id)->first(),
        ];

        return view('addtopic')->with($data);
    }
}
