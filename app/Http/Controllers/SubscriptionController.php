<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribeSubscriptionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**THIS IS THE CONTROLLER THAT ALLOWS SUBSCRIBERS SUBSCRIBE TO THE SYSTEM AND ALSO
 ACCESS THE PRIMIUM CONTENT OF THE SITE **/
 //65e81cd23e77e730afbdf7f18074a3e12840370f46155e87ff0f8b2406801de5
 //51ed553b90adf372fa7c3b839934fb5b77025d851f6a6a2975fbbf4c951c748f
class SubscriptionController extends Controller
{
    public function subscribe(SubscribeSubscriptionRequest $subscribe)
    {
        //validating subscriber phone number

        if (strlen($subscribe->phone) == 10) {
            $phoneNumber = substr($subscribe->phone, 1);
            $phoneNumber = '254'.$phoneNumber;
        } elseif (strlen($subscribe->phone) == 12) {
            $phoneNumber = $subscribe->phone;
        } elseif (strlen($subscribe->phone) == 13) {
            $phoneNumber = substr($subscribe->phone, 1);
            $phoneNumber = $phoneNumber;
        }

        $phone = [
            'phoneNumber' => $phoneNumber,
        ];

        $headers1 = [
            'Accept: application/json',
            'Content-Type: multipart/form-data',
        ];

        $headers = [
            'apiKey: 51ed553b90adf372fa7c3b839934fb5b77025d851f6a6a2975fbbf4c951c748f',
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded',
        ];

        $token = curl_init();
        curl_setopt($token, CURLOPT_URL, 'https://api.africastalking.com/checkout/token/create');
        curl_setopt($token, CURLOPT_POST, true);
        curl_setopt($token, CURLOPT_HTTPHEADER, $headers1);
        curl_setopt($token, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($token, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($token, CURLOPT_POSTFIELDS, $phone);
        $access_token = curl_exec($token);
        curl_close($token);

        $data = json_decode($access_token);

        $inputdata = [
            'username'=> 'admin_ikonews', //multple token array
            'shortCode' => 22384, //single token
            'keyword'=> 'Ikonews',
            'phoneNumber' =>$phoneNumber,
           'checkoutToken' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxx',

        ];

        $subscribe = curl_init();
        curl_setopt($subscribe, CURLOPT_URL, 'https://content.africastalking.com/version1/subscription/create');
        curl_setopt($subscribe, CURLOPT_POST, true);
        curl_setopt($subscribe, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($subscribe, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($subscribe, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($subscribe, CURLOPT_POSTFIELDS, http_build_query($inputdata));
        $result = curl_exec($subscribe);
        curl_close($subscribe);

        echo $result;

        return redirect()->back();
        // return view('site.index')->with('success', 'Confirm subscribtion on your phone');
    }
}
