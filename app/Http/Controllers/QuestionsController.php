<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Question::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'quiz'=>'required',
            'image'=>'image|mimes:jpg,png',
            'answer_image'=>'image|mimes:jpg,png',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $quiz = new Question();

        if ($request->hasFile('image')) {
            $f = $request->file('image');
            $quiz->image = base64_encode(file_get_contents($f->getRealPath()));
        } else {
            $quiz->image = 'null';
        }

        if ($request->hasFile('answer_image')) {
            $f = $request->file('answer_image');
            $quiz->answer_image = base64_encode(file_get_contents($f->getRealPath()));
        } else {
            $quiz->answer_image = 'null';
        }
        //    $checkboxvalue= ($request->has('answer')) ? 'true' : 'false';
        //    $value = $checkboxvalue==='true'?true:false;
        $checkboxvalue = ($request->has('answer')) ? true : false;
        //echo("Value is ".$value);

        $quiz->quiz = $request->input('quiz');
        $quiz->answer = $checkboxvalue;
        $quiz->answer_text = ($request->input('answer_text') == null) ? 'null' : $request->input('answer_text');
        $quiz->topic_id = $request->input('topic_id');
        $quiz->save();
        //store item

        // $response = Question::create($request->all());
        $topic_id = $request->input('topic_id');

        return redirect('/allQuestions/'.$topic_id)->with('success', 'Question added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'quiz'=>'required',
            'image'=>'image|mimes:jpg,png',
            'answer_image'=>'image|mimes:jpg,png',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $quiz = Question::find($id);

        if ($request->hasFile('image')) {
            $f = $request->file('image');
            $quiz->image = base64_encode(file_get_contents($f->getRealPath()));
        } else {
            $quiz->image = $request->input('image_value');
        }

        if ($request->hasFile('answer_image')) {
            $f = $request->file('answer_image');
            $quiz->answer_image = base64_encode(file_get_contents($f->getRealPath()));
        } else {
            $quiz->answer_image = $request->input('answer_image_value');
        }
        //    $checkboxvalue= ($request->has('answer')) ? 'true' : 'false';
        //    $value = $checkboxvalue==='true'?true:false;
        $checkboxvalue = ($request->has('answer')) ? true : false;
        //echo("Value is ".$value);

        $quiz->quiz = $request->input('quiz');
        $quiz->answer = $checkboxvalue;
        $quiz->answer_text = ($request->input('answer_text') == null) ? 'null' : $request->input('answer_text');
        $quiz->topic_id = $request->input('topic_id');
        $quiz->save();
        //store item

        // $response = Question::create($request->all());
        $topic_id = $request->input('topic_id');

        return redirect('/allQuestions/'.$topic_id)->with('success', 'Question editted successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);
        $topicId = $question->topic_id;
        $question->delete();

        return redirect('/allQuestions/'.$topicId)->with('success', 'Question deleted successfully');
    }
}
