<?php

namespace App\Http\Controllers;

use App\Jobs\SMSAlertJob;
use App\Models\SmsAlert;
use App\Util;
use Illuminate\Http\Request;

class SmsAlertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'title' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'link' => 'required',
            'active' => 'nullable',
        ];
        $link = $request->input('link');
        //shortnern the link
        $shortnenUrl = Util::shortenMessage($link);

        $error = \Validator::make($request->all(), $rules);

        if ($error->fails()) {
            return response()->json(['errors' => $error->errors()]);
        }

        //SmsAlert::create($request->all());
        $smsAlert = new SmsAlert();
        $smsAlert->title = $request->input('title');
        $smsAlert->description = $request->input('description').' '.$shortnenUrl;
        $smsAlert->category_id = $request->input('category_id');
        $smsAlert->save();

        //send tweet job
        $postAlerts = new SMSAlertJob();
        $this->dispatch($postAlerts);

        //return response()->json(['success' => 'Data inserted successfully.']);
        return redirect()->to('/sms_alert_list')->with('success', 'Alert created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SmsAlert  $smsAlert
     * @return \Illuminate\Http\Response
     */
    public function show(SmsAlert $smsAlert)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SmsAlert  $smsAlert
     * @return \Illuminate\Http\Response
     */
    public function edit(SmsAlert $smsAlert)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SmsAlert  $smsAlert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmsAlert $smsAlert)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SmsAlert  $smsAlert
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmsAlert $smsAlert)
    {
        //
    }
}
