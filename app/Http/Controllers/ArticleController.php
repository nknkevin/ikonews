<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = DB::table('articles')
                    ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                    ->where('status', 'published')
                    ->select('articles.id as id', 'title', 'slug', 'author', 'status', 'image', 'image_caption', 'category_id', 'article_text', 'article_categories.id as category_id', 'name', 'article_categories.slug as category_slug', 'article_categories.created_at')
                    ->get();

        return $articles; //Article::with('articleCategory')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // 'title',
        // 'author',
        // 'image',
        // 'article_text',
        // 'status'

        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'slug'=>'required|min:3|max:255|unique:articles',
            'article_text'=>'required',
            'image_caption'=>'required',
            'image'=>'required',
            'category_id'=>'required',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //category_id
        if ($request->hasFile('image')) {
            $filenamewithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('image')->storeAs('public/article_images', $fileNameToStore);
        }

        $article = new Article();
        $article->title = $request->input('title');
        $article->slug = $request->input('slug');
        $article->status = 'pending';
        $article->image_caption = $request->input('image_caption');
        if ($request->hasFile('image')) {
            $article->image = $fileNameToStore;
        }
        $article->category_id = $request->input('category_id');
        $article->article_text = $request->input('article_text');
        $article->author = $request->input('author');
        $article->save();

        return redirect()->to('/articleList')->with('success', 'Article added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    public function publish(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(), [
            'title'=>'required',
            'slug'=>'required|min:3|max:255|unique:articles',
            'article_text'=>'required',
            'image_caption'=>'required',
            'category_id'=>'required',

        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //category_id
        //image_caption

        if ($request->hasFile('image')) {
            $filenamewithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenamewithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $path = $request->file('image')->storeAs('public/article_images', $fileNameToStore);
        }

        $article = Article::find($id);
        $article->title = $request->input('title');
        $article->slug = $request->input('slug');
        $article->type = $request->input('type');
        $article->status = 'pending';
        $article->image_caption = $request->input('image_caption');
        if ($request->hasFile('image')) {
            $article->image = $fileNameToStore;
        }
        $article->category_id = $request->input('category_id');
        $article->article_text = $request->input('article_text');
        $article->author = $request->input('author');
        $article->save();

        return redirect('/article/'.$id)->with('success', 'Article added successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }

    //to be deleted as it is just a test
    public function toBeDeleted()
    {
        $title = Article::select('id', 'title')->get();

        foreach ($title as $title) {
            return $title->title;
        }
    }
}
