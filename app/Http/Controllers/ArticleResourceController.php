<?php

namespace App\Http\Controllers;

use App\Models\ArticleResource;
use Illuminate\Http\Request;

class ArticleResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArticleResource  $articleResource
     * @return \Illuminate\Http\Response
     */
    public function show(ArticleResource $articleResource)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArticleResource  $articleResource
     * @return \Illuminate\Http\Response
     */
    public function edit(ArticleResource $articleResource)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArticleResource  $articleResource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArticleResource $articleResource)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArticleResource  $articleResource
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArticleResource $articleResource)
    {
        //
    }
}
