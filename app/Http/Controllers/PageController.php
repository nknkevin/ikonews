<?php

namespace App\Http\Controllers;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Jobs\FacebookPost;
use App\Jobs\SendSocialMediaAlert;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\ArticleComment;
use App\Models\ArticleResource;
use App\Models\SmsAlert;
use App\Models\User;
use App\Util;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    //fb
    public function sendFb()
    {
        $appId = '262391675281119';
        $appSecret = '7c5e0c66a70f3ba2a89cc056bc8ee7ca';
        $pageId = '104485891679371';
        $userAccessToken = 'EAADupNKY3t8BAFY9lCze1GyVaX1HWx6Bq5uZAz8WibNxAnF7gzzgiXZCRNq1RZA7EECzX6K1YHZBWYewZAYKHVtZC2ybwfOaJHXwrFZAhQvLZCQy4xdzlCDyQZChPnZCSqnUwlQMkM0S4eGll20lrjqZAI9DPtj1ZAZCPieEMOeMzv1fvuZAdqGJC0nrbiwFBXuNgFW0nYSjy8Ml0bLIZBEsdhBvKlYubaeTc8ZCkrb5r7YlZAcAJQwZDZD';
        /*
                $fb = new Facebook([
                    'app_id' => $appId,
                    'app_secret' => $appSecret,
                    'default_access_token' => $userAccessToken,
                    'default_graph_version' => 'v2.5'
                ]);

                $longLivedToken = $fb->getOAuth2Client()->getLongLivedAccessToken($userAccessToken);

                $fb->setDefaultAccessToken($longLivedToken);

                $response = $fb->sendRequest('GET', $pageId, ['fields' => 'access_token'])
                    ->getDecodedBody();

                $foreverPageAccessToken = $response['access_token'];

                $helper = $fb->getRedirectLoginHelper();

                try {
                $accessToken = $helper->getAccessToken();
                $response = $fb->get('/me?fields=id,name', $accessToken );

                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                }

                $fb->setDefaultAccessToken($accessToken);
                $result = $fb->sendRequest('POST', "$pageId/feed", [
                    'message' => 'Fresh and Precise',
                    'link' => 'https://www.today.ikonews.co.ke/',
                ]);

                return serialize($result);

        */
    }

    //tweet
    public function sendTweet()
    {
        define('CONSUMER_KEY', 'dhT308AZxEMQ7FWNc801Rt13M');
        define('CONSUMER_SECRET', 'PAwlR3CabyMd6JCCCUsv7cpBjeOmAV30BmKhNrsSyqVbu7W2YU');
        define('ACCESS_TOKEN', '1353594887799795713-vJeYumVQaVXaYrmdHsRd1W9ovKbVxE');
        define('ACCESS_TOKEN_SECRET', 'KIdKWvyjS0iaPlih6SwqYYFoKNTPAETyStV8vTXGsi9Uc');
        /*
                $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

                $path = Storage::disk('public')->path('article_images/ikonews_1611044525.PNG');//Storage::path('articles/ikonews_1611044525.PNG');

                //return serialize($path);
                $media1 = $connection->upload('media/upload', ['media' =>$path]);
                $parameters = [
                    'status' => 'Hello Word with Images! https://artisansweb.net',
                    'media_ids' => implode(',', [$media1->media_id_string])
                ];
                $result = $connection->post('statuses/update', $parameters);

                // $link=url("/read_article/5");
                // $status='Political Parties Disputes Tribunal Has Suspended The Decision By Jubilee Party To Expel Six Senators .Read more: '.$link;

                // $sendSocialMediaAlertJob = new SendSocialMediaAlert("ikonews_1611044525.PNG",$status,$link);
                // $this->dispatch($sendSocialMediaAlertJob);

                //$content = $connection->get("account/verify_credentials");
                //return "Tweet made";
                return serialize($result);
                */
    }

    //Article::with('articleCategory')->get();
    //siteHome
    public function siteHome()
    {
        $featuredArticle = Article::with('articleCategory')->get();

        $featuredArticle = DB::table('articles')
                                ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                                ->where('status', 'published')
                                ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                                ->orderByDesc('created_at')
                                ->first();
        $popular = DB::table('articles')
                    ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                    ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                    ->where('status', 'published')
                    ->orderByDesc('created_at')
                    ->take(3)
                    ->get();

        $powerplay = DB::table('articles')
                    ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                    ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                    ->where('status', 'published')
                    ->where('category_id', '8')
                    ->orderByDesc('created_at')
                    ->take(2)
                    ->get();

        $articles = DB::table('articles')
                    ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                    ->where('status', 'published')
                    ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                    ->orderByDesc('created_at')
                    ->paginate(16);

        $sponsored = DB::table('articles')
                    ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                    ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                    ->where('status', 'published')
                    ->where('type', 'sponsored')
                    ->orderByDesc('created_at')
                    ->take(3)
                    ->get();

        return view('site.index', ['featuredArticle' => $featuredArticle,
                    'popular' => $popular,
                    'powerplay' => $powerplay,
                    'articleList' => $articles,
                    'sponsored'=>$sponsored, ]);
    }

    //dashboard
    public function dashboard()
    {
        $data = [
            'categories' =>ArticleCategory::orderBy('created_at')->get(),
            'articles'=>Article::orderBy('created_at')->get(),
            'users'=>User::orderBy('created_at')->get(),
        ];

        return view('dashboard')->with($data);
    }

    //editorDashboard
    public function editorDashboard()
    {
        $data = [
            'categories' =>ArticleCategory::orderBy('created_at')->get(),
            'articles'=>Article::orderBy('created_at')->get(),
            'pending_articles'=>Article::where('status', 'pending')->get(),
            'published_articles'=>Article::where('status', 'published')->get(),
            'users'=>User::orderBy('created_at')->get(),
        ];

        return view('editor_dashboard')->with($data);
    }

    //reporterDashboard
    public function reporterDashboard()
    {
        $data = [
            'categories' =>ArticleCategory::orderBy('created_at')->get(),
            'pending_articles'=>Article::where('status', 'pending')->get(),
            'published_articles'=>Article::where('status', 'published')->get(),
            'articles'=>Article::orderBy('created_at')->get(),
            'users'=>User::orderBy('created_at')->get(),
        ];

        return view('reporter_dashboard')->with($data);
    }

    //pending article
    //published_article orderBy('created_at', 'desc')

    //
    public function editorArticlesList($type)
    {
        switch ($type) {
            case 'pending':
                $data = Article::where('status', 'pending')->orderByDesc('created_at')->paginate(20);

                return view('editor_pending_articles_list')->with('data', $data);
            break;
            case 'published':
                $data = Article::where('status', 'published')->orderByDesc('created_at')->paginate(20);

                return view('editor_published_articles_list')->with('data', $data);
                break;
    }
    }

    //publishArticle
    public function publishArticle($id)
    {
        $article = Article::find($id);
        $article->status = 'published';
        $article->save();

        //publish article to social media sites
        $link = url("/read_article/{$article->id}");
        $status = $article->title.' Read more: '.$link;
        /*
                //send tweet job
                $sendSocialMediaAlertJob = new SendSocialMediaAlert($article->image,$status);
                $this->dispatch($sendSocialMediaAlertJob);

                //send the Post to FaceBook
                $postToFacebookJob = new FacebookPost($article->image,$status);
                $this->dispatch($postToFacebookJob);

        */

        return redirect('/editorArticle/published/'.$id);
    }

    public function registerUser()
    {
        return view('auth.register_user');
    }

    public function editorArticleItem($type, $article_id)
    {
        switch ($type) {
            case 'pending':
                $data = ['article' =>Article::find($article_id)];

                return view('editor_pending_article_item')->with($data);

            break;
            case 'published':
                $data = ['article' =>Article::find($article_id)];

                return view('editor_published_article_item')->with($data);

                break;

    }
    }

    public function delete($article_id)
    {
        $delete = Article::find($article_id)->delete();
        if ($delete) {
            $type = 'published';

            return redirect('editorArticles/'.$type);
        } else {
            abort(500);
        }
    }

    public function deleteunpublished($article_id)
    {
        $delete = Article::find($article_id)->delete();
        if ($delete) {
            $type = 'pending';

            return redirect('editorArticles/'.$type);
        } else {
            abort(500);
        }
    }

    //reporter
    public function reporterArticlesList($type)
    {
        switch ($type) {
            case 'pending':
                $data = Article::orderByDesc('created_at')->where('status', 'pending')->paginate(20);

                return view('pending_articles_list')->with('data', $data);
            break;
            case 'published':
                $data = Article::orderByDesc('created_at')->where('status', 'published')->paginate(20);

                return view('published_articles_list')->with('data', $data);
                break;
    }
    }

    //reporter item
    public function reporterArticleItem($type, $article_id)
    {
        switch ($type) {
            case 'pending':
                $data = ['article' =>Article::find($article_id)];

                return view('article_item')->with($data);

            break;
            case 'published':
                $data = ['article' =>Article::find($article_id)];

                return view('published_article_item')->with($data);

                break;
    }
    }

    //add article
    public function newArticle()
    {
        $data = [
            'categories' =>ArticleCategory::orderByDesc('created_at')->get(),
        ];

        return view('new_article')->with($data);
    }

    //view articles
    public function articlesList()
    {
        $data = Article::orderByDesc('created_at')->paginate(20);

        return view('articles_list')->with('data', $data);
    }

    //read article - id
    public function articleRead($article_id)
    {
        $data = [
            'articles'=>Article::orderByDesc('created_at')->paginate(2),
            'featuredArticle'=>Article::orderByDesc('created_at')->first(),
            'latestNewsArticles'=>DB::table('articles')
                                ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                                ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                                ->take(3)
                                ->orderByDesc('created_at')
                                ->get(),
            'articleItem' =>DB::table('articles')
                        ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                        ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                        ->where('articles.id', $article_id)->first(),

        ];

        return view('site.article_read')->with($data);
    }

    //read article - slug
    public function articleReadSlug($category_slug, $readarticle_slug)
    {
        $data = [
            'articles'=>Article::orderByDesc('created_at')->paginate(2),
            'featuredArticle'=>Article::orderByDesc('created_at')->first(),
            'latestNewsArticles'=>DB::table('articles')
                                ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                                ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                                ->take(3)
                                ->orderByDesc('created_at')
                                ->get(),
            'articleItem' =>DB::table('articles')
                        ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                        ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                        ->where('articles.slug', $readarticle_slug)->first(),

        ];
        //return $data;
        return view('site.article_read')->with($data);
    }

    //articleItem
    public function articleItem($article_id)
    {
        $data = [
            'article' =>Article::find($article_id),
        ];

        return view('article_item')->with($data);
    }

    //articleItem-slug
    public function articleItemSlug($article_slug)
    {
        $data = [
            'article' =>Article::find($article_slug),
        ];

        return view('article_item')->with($data);
    }

    //articleSiteCategory
    public function articleSiteCategory($category_id)
    {
        $data = [
            'articles' =>DB::table('articles')
                                ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                                ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                                ->where('articles.category_id', $category_id)
                                ->orderByDesc('created_at')
                                ->paginate(4),
            'featuredArticle'=>Article::orderByDesc('created_at')->first(),
            'latestNewsArticles'=>Article::orderByDesc('created_at')->take(3)->get(),
            'category' =>ArticleCategory::find($category_id),
        ];

        return view('site.article_category')->with($data);
    }

    //articleSiteCategory Slug
    public function articleSiteCategorySlug($readcategory_slug)
    {
        $data = [
            'articles'=>DB::table('articles')
                                ->join('article_categories', 'articles.category_id', '=', 'article_categories.id')
                                ->select('articles.id as article_id', 'title', 'articles.slug as article_slug', 'author', 'status', 'image', 'image_caption', 'category_id as category_id', 'article_text', 'article_categories.id', 'name', 'article_categories.slug as category_slug', 'articles.created_at as created_at')
                                ->where('article_categories.slug', $readcategory_slug)
                                ->orderByDesc('created_at')
                                ->paginate(4),
            'featuredArticle'=>Article::orderByDesc('created_at')->first(),
            'latestNewsArticles'=>Article::orderByDesc('created_at')->take(3)->get(),
            'category'=>DB::table('article_categories')->where('slug', $readcategory_slug)->value('name'),
        ];
        //return $data;
        return view('site.article_category')->with($data);
    }

    public function editArticleItem($article_id)
    {
        $data = [
            'article' =>Article::find($article_id),
            'categories' =>ArticleCategory::orderByDesc('created_at')->get(),
        ];

        return view('edit_article')->with($data);
    }

    public function editReporterArticleItem($article_id)
    {
        $data = [
            'article' =>Article::find($article_id),
            'categories' =>ArticleCategory::orderByDesc('created_at')->get(),
        ];

        return view('reporter_edit_article')->with($data);
    }

    //edit article

    //add article categories

    public function newCategory()
    {
        return view('add_article_category');
    }

    //edit category
    public function editCategory($id)
    {
        $data = [
            'category' =>ArticleCategory::find($id),
        ];

        return view('edit_article_category')->with($data);
    }

    //see categoriy
    public function listCategories()
    {
        $data = ArticleCategory::orderByDesc('created_at')->paginate(20);

        return view('categories_list')->with('data', $data);
    }

    //edit article categories

    //sms alerts
    //new alert
    public function smsAlertList()
    {
        $data = SmsAlert::orderByDesc('created_at')->paginate(20);

        return view('smsalerts_list')->with('data', $data);
    }

    //alert list
    public function newSmsAlert()
    {
        return view('create_smsalerts');
    }
}
