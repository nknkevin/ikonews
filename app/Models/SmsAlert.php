<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmsAlert extends Model
{
    //
    protected $fillable = ['title', 'description', 'category_id', 'user_id', 'active', 'send_status'];
}
