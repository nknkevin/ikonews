<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleResource extends Model
{
    //
    protected $fillable = [
        'name',
        'type',
        'file_name',
        'article_id',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
