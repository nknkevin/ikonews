<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteTopic extends Model
{
    protected $fillable = [
        'name',
        'subject_id',
        'type',
        'form',
    ];

    public function subject()
    {
        return $this->belongsTo(NoteSubject::class);
    }

    public function question()
    {
        return $this->hasMany(NotePage::class);
    }
}
