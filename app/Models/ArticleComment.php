<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleComment extends Model
{
    protected $fillable = [
        'comment',
        'commented_by',
        'user_id',
    ];

    //
}
