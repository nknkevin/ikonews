<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotePage extends Model
{
    protected $fillable = [
        'text',
        'page_no',
    ];

    public function topic()
    {
        return $this->belongsTo(NoteTopic::class);
    }
}
