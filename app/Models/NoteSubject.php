<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoteSubject extends Model
{
    protected $fillable = [
        'name',
    ];

    public function topic()
    {
        return $this->hasMany(NoteTopic::class);
    }
}
