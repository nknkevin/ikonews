<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'slug',
        'author',
        'image',
        'article_text',
        'status',
        'image_caption',
        'category_id',
    ];

    public function articleResource()
    {
        return $this->hasMany(ArticleResource::class);
    }

    public function articleAlias()
    {
        return 'slug';
    }

    public function articleCategory()
    {
        return $this->belongsTo(ArticleCategory::class, 'category_id', 'id');
    }
}
