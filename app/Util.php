<?php

namespace App;

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Storage;

class Util
{
    public static function truncateStrings(string $string, $limit = 85)
    {
        $break = '.';
        $pad = '...';
        if (strlen($string) <= $limit) {
            return $string;
        }

        // is $break present between $limit and the end of the string?
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint).$pad;
            }
        }

        return $string;
    }

    public static function sendTweet($articleImage, $status)
    {
        define('CONSUMER_KEY', 'dhT308AZxEMQ7FWNc801Rt13M');
        define('CONSUMER_SECRET', 'PAwlR3CabyMd6JCCCUsv7cpBjeOmAV30BmKhNrsSyqVbu7W2YU');
        define('ACCESS_TOKEN', '1353594887799795713-vJeYumVQaVXaYrmdHsRd1W9ovKbVxE');
        define('ACCESS_TOKEN_SECRET', 'KIdKWvyjS0iaPlih6SwqYYFoKNTPAETyStV8vTXGsi9Uc');

        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

        $path = Storage::disk('public')->path('article_images/'.$articleImage);

        //return serialize($path);
        $media1 = $connection->upload('media/upload', ['media' =>$path]);
        $parameters = [
            'status' => $status,
            'media_ids' => implode(',', [$media1->media_id_string]),
        ];
        $result = $connection->post('statuses/update', $parameters);
    }

    //url shortner
    public static function shortenMessage($long_url)
    {
        $shortenUrl = '';
        $apiv4 = 'https://api-ssl.bitly.com/v4/bitlinks';
        $token = 'b426f5dcdc389e0e6cbffcb80476e7fa4391d725';
        $genericAccessToken = $token;

        $data = [
            'long_url' => $long_url,
        ];
        $payload = json_encode($data);

        $header = [
            'Authorization: Bearer '.$genericAccessToken,
            'Content-Type: application/json',
            'Content-Length: '.strlen($payload),
        ];

        $ch = curl_init($apiv4);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result = curl_exec($ch);
        $resultToJson = json_decode($result);

        if (isset($resultToJson->link)) {
            $shortenUrl = $resultToJson->link;
        } else {
            $shortenUrl = 'n/a';
        }

        return $shortenUrl;
    }
}
