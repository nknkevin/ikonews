<?php

use App\Http\Controllers\ArticleCategoryController;
use App\Http\Controllers\ArticleCommentController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ArticleResourceController;
use App\Http\Controllers\SmsAlertController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::resource('question','QuestionsController');

//iko news
Route::resource('category', ArticleCategoryController::class);
Route::resource('article', ArticleController::class);
Route::resource('article_comments', ArticleCommentController::class);
Route::resource('article_resource', ArticleResourceController::class);
Route::resource('smsalerts', SmsAlertController::class);
//publish
Route::PUT('publish/{id}', [ArticleController::class, 'publish']);
