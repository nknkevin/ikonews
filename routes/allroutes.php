<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//iko news route

Route::get('/', [PageController::class, 'siteHome']);

Route::get('/dashboard', [PageController::class, 'dashboard']);

Route::get('category', [PageController::class, 'newCategory']);
//listCategories
Route::get('categoryList', [PageController::class, 'listCategories']);

//add article
Route::get('newArticle', [PageController::class, 'newArticle']);

//list articles
Route::get('articleList', [PageController::class, 'articlesList']);

//article list
Route::get('article/{article_id}', [PageController::class, 'articleItem']);

//editArticleItem
Route::get('editArticle/{article_id}', [PageController::class, 'editArticleItem']);
//editCategory
Route::get('editCategory/{id}', [PageController::class, 'editCategory']);
//edit reporter article
Route::get('editMyArticle/{article_id}', [PageController::class, 'editReporterArticleItem']);

//articleRead
Route::get('read_article/{id}', [PageController::class, 'articleRead']);
Route::get('publish/{id}', [PageController::class, 'publishArticle']);
//article category
Route::get('article_category/{id}', [PageController::class, 'articleSiteCategory']);

Route::get('editor', [PageController::class, 'editorDashboard']);
Route::get('reporter', [PageController::class, 'reporterDashboard']);

//editorArticleItem
Route::get('editorArticle/{type}/{id}', [PageController::class, 'editorArticleItem']);
Route::get('editorArticles/{type}', [PageController::class, 'editorArticlesList']);

Route::get('reporterArticle/{type}/{id}', [PageController::class, 'reporterArticleItem']);
Route::get('reporterArticles/{type}', [PageController::class, 'reporterArticlesList']);
Route::get('/tweet', [PageController::class, 'sendTweet']);
Route::get('/fb', [PageController::class, 'sendFb']);
Route::get('/sms_alert', [PageController::class, 'newSmsAlert']);
Route::get('/sms_alert_list', [PageController::class, 'smsAlertList']);

//tweet
// Route::get('/tweet', function()
// {
// 	//$uploaded_media = Twitter::uploadMedia(['media' => File::get("https://today.ikonews.co.ke/website/images/logo.png")]);
//     //$uploaded_media = Twitter::uploadMedia(['media' => File::get(public_path('filename.jpg'))]); //getUrl

//     //return Twitter::postTweet(['status' => 'Iko News. Fresh and Precise', 'format' => 'json']);
// });

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/userregistration', [PageController::class, 'registerUser'])->name('IkoNews | Register');
