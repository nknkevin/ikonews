<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\SubscriptionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::middleware(['none'])->group(function () {

    //Editor dashboard
    Route::get('/dashboard', [PageController::class, 'dashboard']);

    //Adding a new News category
    Route::get('category', [PageController::class, 'newCategory']);

    //listCategories
    Route::get('categoryList', [PageController::class, 'listCategories']);

    //add article
    Route::get('newArticle', [PageController::class, 'newArticle']);

    //list articles
    Route::get('articleList', [PageController::class, 'articlesList']);

    //article list
    Route::get('article/{article_id}', [PageController::class, 'articleItem']);

    //editArticleItem
    Route::get('editArticle/{article_id}', [PageController::class, 'editArticleItem']);

    //editCategory
    Route::get('editCategory/{id}', [PageController::class, 'editCategory']);

    //edit reporter article
    Route::get('editMyArticle/{article_id}', [PageController::class, 'editReporterArticleItem']);

    //Editors route
    Route::get('editor', [PageController::class, 'editorDashboard']);

    //Reporters route
    Route::get('reporter', [PageController::class, 'reporterDashboard']);

    //editorArticleItem
    Route::get('editorArticle/{type}/{id}', [PageController::class, 'editorArticleItem']);
    Route::get('editorArticles/{type}', [PageController::class, 'editorArticlesList']);

    Route::get('reporterArticle/{type}/{id}', [PageController::class, 'reporterArticleItem']);
    Route::get('reporterArticles/{type}', [PageController::class, 'reporterArticlesList']);
    Route::get('/tweet', [PageController::class, 'sendTweet']);
    Route::get('/fb', [PageController::class, 'sendFb']);
    Route::get('/sms_alert', [PageController::class, 'newSmsAlert']);
    Route::get('/sms_alert_list', [PageController::class, 'smsAlertList']);

    //deleting an article
    Route::get('delete/{article_id}', [PageController::class, 'delete'])->name('delete.article');

    //deleting any unpublished article
    Route::get('delete/pending/{article_id}', [PageController::class, 'deleteunpublished'])->name('delete.unpublished.article');

// });

//iko news route

Route::get('/', [PageController::class, 'siteHome']);

Route::POST('subscribe', [SubscriptionController::class, 'subscribe'])->name('subscribe');

//articleRead
Route::get('read_article/{id}', [PageController::class, 'articleRead']);

Route::get('publish/{id}', [PageController::class, 'publishArticle']);
//article category
Route::get('article_category/{id}', [PageController::class, 'articleSiteCategory']);

//SEO article reading
Route::get('read_article/{article_slug}', [PageController::class, 'articleReadSlug']);
Route::get('category/{category_slug}', [PageController::class, 'articleSiteCategorySlug']);
Route::get('{category_slug}/{article_slug}', [PageController::class, 'articleReadSlug']);

//tweet
// Route::get('/tweet', function()
// {
// 	//$uploaded_media = Twitter::uploadMedia(['media' => File::get("https://today.ikonews.co.ke/website/images/logo.png")]);
//     //$uploaded_media = Twitter::uploadMedia(['media' => File::get(public_path('filename.jpg'))]); //getUrl

//     //return Twitter::postTweet(['status' => 'Iko News. Fresh and Precise', 'format' => 'json']);
// });

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/userregistration', [PageController::class, 'registerUser'])->name('IkoNews | Register');

Route::get('toBeDeleted', [ArticleController::class, 'toBeDeleted']);
